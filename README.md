Prestashop optimized product query
======================

Replace ExecuteS() method in Db Layer.

New method, remove usually unused Description, Short Description, Meta Keyword, Keywords fields from SQL query on newproducts / category / product / advancedsearch4
controllers.

On huge catalog, with filled descriptions, query exec time fall down around 0,5s. Only because blob fields have an heavy cost on query data transfert.

Installation
============
Create folder optimizedqueries in modules folder. Download and copy all files from repo and install module from Prestashop.
Update cache from backoffice or delete cache/class_index.php file
