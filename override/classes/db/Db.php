<?php
abstract class Db extends DbCore
{
    /**
     * Executes return the result of $sql as array
     *
     * @param string|DbQuery $sql Query to execute
     * @param bool $array Return an array instead of a result object (deprecated since 1.5.0.1, use query method instead)
     * @param bool $use_cache
     * @return array|false|null|mysqli_result|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    /*
    * module: optimizedqueries
    * date: 2016-05-07 02:27:30
    * version: 1.0
    */
    public function executeS($sql, $array = true, $use_cache = true)
    {
        if ($sql instanceof DbQuery) {
            $sql = $sql->build();
        }

        $list_controller = array('newproducts', 'category', 'product', 'advancedsearch4');
        if (in_array(Tools::getValue('controller'), $list_controller)) {
            if (strstr($sql, 'LIMIT') &&
                (strstr($sql, 'pl.`description`, pl.`description_short`,')
                    ||
                    strstr($sql, 'pl.description, pl.description_short,')
                )
            ) {
                $to_replace = array(
                    'pl.`description`, pl.`description_short`,',
                    'pl.description, pl.description_short,',
                    'pl.`meta_description`, pl.`meta_keywords`,',
                    'pl.meta_description, pl.meta_keywords,'
                );
                $replace_by = array(
                    '/*pl.`description`, pl.`description_short`,*/',
                    '/*pl.description, pl.description_short,*/',
                    '/*pl.`meta_description`, pl.`meta_keywords`,*/',
                    '/*pl.meta_description, pl.meta_keywords,*/'
                );

                $sql = str_replace($to_replace, $replace_by, $sql);
            }
        }

        $this->result = false;
        $this->last_query = $sql;
        if ($use_cache && $this->is_cache_enabled && $array) {
            $this->last_query_hash = Tools::encryptIV($sql);
            if (($result = Cache::getInstance()->get($this->last_query_hash)) !== false) {
                $this->last_cached = true;
                return $result;
            }
        }
        if (!preg_match('#^\s*\(?\s*(select|show|explain|describe|desc)\s#i', $sql)) {
            if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_) {
                throw new PrestaShopDatabaseException('Db->executeS() must be used only with select, show, explain or describe queries');
            }
            return $this->execute($sql, $use_cache);
        }
        $this->result = $this->query($sql);
        if (!$this->result) {
            $result = false;
        } else {
            if (!$array) {
                $use_cache = false;
                $result = $this->result;
            } else {
                $result = $this->getAll($this->result);
            }
        }
        $this->last_cached = false;
        if ($use_cache && $this->is_cache_enabled && $array) {
            Cache::getInstance()->setQuery($sql, $result);
        }
        return $result;
    }
}
