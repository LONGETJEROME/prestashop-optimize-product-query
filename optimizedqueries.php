<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class optimizedqueries extends Module
{
    public function __construct()
    {
        $this->name = 'optimizedqueries';
        $this->author = 'Home-Made';
        $this->tab = 'front_office_features';
        $this->version = '1.0';

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        $this->displayName = $this->l('Optimize Product SQL queries');
        $this->description = $this->l('Optimize Product SQL queries on categories pages');
        $this->confirmUninstall = $this->l('Do you want to uninstall module ' . $this->displayName);

        parent::__construct();
    }
}
