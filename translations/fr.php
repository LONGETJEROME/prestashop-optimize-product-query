<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{optimizedqueries}prestashop>optimizedqueries_abf5803b43849ed22dfd40aedf2ac4e4'] = 'Optimisation des requêtes SQL produit';
$_MODULE['<{optimizedqueries}prestashop>optimizedqueries_cd0838f27207ff92b1b40585f816d38e'] = 'Sur de gros catalogues à partir de 4000 produits, gagnez jusqu\'à 1/2 seconde sur vos requêtes sur les pages catégorie';
